from django.db import models

class Articl(models.Model):
    title = models.CharField('Название' , max_length=50)
    info = models.CharField('Информация' , max_length=250)
    full_text = models.TextField('Текст')
    data = models.DateTimeField('Дата публикации')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return f'/vacanse/{self.id}'

    class Meta:
        verbose_name = 'Вакансия'
        verbose_name_plural = 'Вакансии '