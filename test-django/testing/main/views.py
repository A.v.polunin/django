from distutils.log import error
from importlib.resources import path
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Articl
from .forms import ArticlForm
from django.views import View
from django.contrib.auth import authenticate , login
from main.forms import UserCreationForm
from django.views.generic import DetailView , UpdateView , DeleteView

class register(View):

    template_name = 'registration/register.html'

    def get(self,request):
        context = {
            'form' : UserCreationForm()
        }
        return render(request, self.template_name , context)
    
    def post(self , request):
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            return redirect('login')
        context = {
            'form' : form
        }
        return render(request, self.template_name , context)

class VacanseDetailView(DetailView):
    model = Articl
    template_name = 'main/vacansePodrobno.html'
    context_object_name = 'articles'

class VacanseUpdateView(UpdateView):
    model = Articl
    template_name = 'main/addform.html'

    form_class = ArticlForm

class VacanseDeleteView(DeleteView):
    model = Articl
    success_url = '/vacanse/'
    template_name = 'main/delete.html'

def index(request): 
    data = {
        'title' : 'Главная страница ',
        'values': ['some' , 'people' , 'hello'],
        'obj': {
            'date': '18.03',
            'age': '18',
            'car': 'bmw'
        }
    }
    return render(request , 'main/index.html' , data)


def vacanse(request):
    search_query = request.GET.get('search' , '')
    if search_query:
        vacanses = Articl.objects.filter(title__icontains = search_query)
    else:
        vacanses = Articl.objects.all()
    return render(request , 'main/vacanse.html' , {'vacanses': vacanses})

def vacansePodrobno(request):
    vacanses = Articl.objects.all()
    return render(request , 'main/vacansePodrobno.html' , {'vacanses': vacanses})

def rabotadatel(request):
    return render(request , 'main/rabotadatel.html')

def vacanseRabotadatel(request):
    vacanses = Articl.objects.all()
    return render(request , 'main/vacanseRabotadatel.html' , {'vacanses': vacanses})

def addform(request):
    error = ''
    if request.method == 'POST':
        form = ArticlForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('vacanse')
        else:
            error = 'Форма заполнена не верно'

    form = ArticlForm()

    data = {
        'form' : form,
        'error' : error
    }

    return render(request , 'main/addform.html' , data )